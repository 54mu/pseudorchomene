# Trascrittomica *Pseudorchomene*

## QC

After removing the *Eusirus* sample all other samples were re-cecked for quality with fastQC + multiQC

## Trimming

Trimming of reads was performed using fastp 0.20.0 with the following options:

```bash 
fastp  -i read.1.fastq.gz -I read.2.fastq.gz -o read.1.Trimmed.fastq.gz \
-O read.2.Trimmed.fastq.gz -f 11 -t 5 -g -x -5 -3 -n 2 -l 75 -w 16 -V  
```

## Normalization

Performed with bbnorm.sh v37.62 

```bash
bbnorm.sh in=read1 in2=read2 out1=read1.norm out2=read2.norm prefilter=t fs=t max=100 min=1
```

## Assembly

Reads were assembled with the Oyster River Protocol 2.3.1

```
oyster.mk STRAND=RF TPM_FILT=1 MEM=400 CPU=64 READ1=Pseudorchomene_norm.1.fastq READ2=Pseudorchomene_norm.2.fastq RUNOUT=Pseudorchomene
```

### Assembly quality

Assembly quality was evaluated via N50 and E90N50

```
Total contigs: 247997

Contig N50: 697

Median contig length: 316
Average contig: 522.07
Total assembled bases: 129472276

E90N50: 1808

E95N50: 1515
```

![plot della ExN50 per ogni Ex](img/20200623091741.png)


### Assembly completeness

Evlauated with BUSCO v.3

```bash 

# BUSCO against Metazoa 
C:77.3%[S:58.2%,D:19.1%],F:15.8%,M:6.9%,n:978 

# BUSCO against Arthropoda
C:70.2%[S:55.8%,D:14.4%],F:19.9%,M:9.9%,n:1066

```

### Assembly optimization

#### removal of sequences shorter than 251 nucleotides

> 78566 sequences were discarded  

```

BUSCO against Arthropoda
C:70.8%[S:56.0%,D:14.8%],F:16.7%,M:12.5%,n:1066

```

#### removal of sequences shorter than 301 nucleotides

> 115670 seqquences were discarded 

```

BUSCO against Arthropoda
C:70.5%[S:56.0%,D:14.5%],F:14.6%,M:14.9%,n:1066

E90N50: 1853

E95N50: 1610

```

![ExN50 plot of the 300+ filtered transcriptome](img/20200623114503.png)

By removing short sequences the lost BUSCOS are all from the fragmented ones. 

## DEG analisys

### Read mapping 

Performed using Salmon v.xx on the transcriptome filtered by length (300+)  

### Sample scatter plots

Control T6 comparison

![20200624141837](img/20200624141837.png)

Stressed T6 Comparison

![20200624142022](img/20200624142022.png)

Control T7 Comparison

![20200624142110](img/20200624142110.png)

Stressed T7 Comparison

![20200624142146](img/20200624142146.png)

## Contaminants:

### Campione 21

![20200625160130](img/20200625160130.png)

#### Blast a campione

da blast a campione di sequenze nella lingua adiacente all'asse x del grafico:

> 21 : *Hyalella azteca*

#### Arricchimento GO

i GO più arricchiti riguardano la chitina e la cuticola degli artropodi e in un altro campione sono arricchiti GO riguardanti vari organelli, tra cui alcuni che non dovrebbero esserci come vacuolo e plastidi.

#### Arricchimento PFAM

oltre a chitina e cuticola in alcuni campioni vengono fuori pfam appartenenti a piante o batteri


### Campione 34

#### Blast a campione

dai blast a campione non si capisce bene, sembra un batterio o una pianta

#### Arricchimento GO

#### Arricchimento PFAM

### Espressione di COI

usando la COI umana per una ricerca di BLAST nel trascrittoma assemblato e incrociando i risultati con evalue < 1e-50 con i valori di espressione per signolo campione le due sequenze COI con l'espressione più alta sono:

```bash

$ for file in `find trim/expr/mapping_300+ -name "*.expr"`
do
echo $file; grep -f COI_hit_list $file/quant.sf | sort -n -k 5 | cut -f1,5 | tail -n 3 
echo "-------------"
done
trim/expr/mapping_300+/22-C-T6.expr
R8089589        589.114
R8084819        888.919
NODE_79_length_6057_cov_406.881539_g9_i2        62688.996
-------------
trim/expr/mapping_300+/31-C-T7.expr
TRINITY_DN91165_c3_g1_i1        85.000
R8084819        108.412
NODE_79_length_6057_cov_406.881539_g9_i2        9320.990
-------------
trim/expr/mapping_300+/37-T-T7.expr
R8089589        679.003
R8084819        1234.594
NODE_79_length_6057_cov_406.881539_g9_i2        168904.861
-------------
trim/expr/mapping_300+/Rif_4.expr
R8089589        237.000
R8084819        437.595
NODE_79_length_6057_cov_406.881539_g9_i2        58955.137
-------------
trim/expr/mapping_300+/28-T-T6.expr
R8089589        922.470
R8084819        1617.239
NODE_79_length_6057_cov_406.881539_g9_i2        334753.188
-------------
trim/expr/mapping_300+/27-T-T6.expr
R8089589        522.024
R8084819        987.354
NODE_79_length_6057_cov_406.881539_g9_i2        119076.217
-------------
trim/expr/mapping_300+/40-T-T7.expr
R8083447        471.142
R8084819        795.121
NODE_79_length_6057_cov_406.881539_g9_i2        60195.837
-------------
trim/expr/mapping_300+/34-C-T7.expr
R8084819        1041.021
R8083447        1122.117
NODE_79_length_6057_cov_406.881539_g9_i2        39547.831
-------------
trim/expr/mapping_300+/21-C-T6.expr
R8083447        3505.587
R8084819        5833.759
NODE_79_length_6057_cov_406.881539_g9_i2        241015.325

```

La sequenza più espressa è quella che probabilmente appartiene allo Pseudorchomene, mentre nei due campioni "peggiori" la sequenza R8083447 risulta molto espressa. Questa sequenza appertiene ad un parassita dei crostacei "*Hematodinium*" con una similarità della COI > 97%. 

La NODE_79_length_6057_cov_406.881539_g9_i2 invece ha il balst migliore su *Hirondellea gigas*, ma con una similarità 79.88% e con query coverage 13%. Questa è la sequenza che dovrebbe appartenere alla specie di interesse.

Facendo BlastP delle ORF si trova:

![20200626091753](img/20200626091753.png)

### Kraken2

con kraken2 ho controllato tutti i fiel di R1 delle read di assemblaggio e verificato la presenza di read di *Hematodinium*. 

Le proporzioni tra il totale delle read identificate e quelle identificate come Heatodinium sono:

|	|Hem.	|total_mapped	|perc.|
|-------|-------|---------------|-----|
|21	|71639	|14111587	|0.507660832194139|
|22	|1963	|1578117	|0.124388749376631|
|27	|187	|2216810	|0.008435544769286|
|28	|25	|7902251	|0.000316365552043|
|31	|2	|185490	|0.001078225241253|
|34	|22201	|1615133	|1.37456172339987|
|37	|39	|3962948	|0.00098411586526|
|40	|7747	|2039454	|0.379856569454374|
|rif4	|3	|1542839	|0.000194446730994|

in almeno 3 campioni la percentuale di contaminazione è molto più alta che negli altri. 

### Assemblaggio trascrittoma di *Hematodinium*

C'erano reads SRA 

> SRR3405005  
> SRR3405006  
> SRR3405007  

### Decontaminazione trascrittoma

per rimuovere le sequenze del parassita si usano blast su vari database per confrontare i risultati: [questo articolo](https://bmcgenomics.biomedcentral.com/articles/10.1186/s12864-015-2039-6) è interessante 

![20200707133738](img/20200707133738.png)

>Rather than use assembly contigs as queries against databases of the myxozoan and host sets, a database of the assembled contigs was generated, against which all BLAST queries were implemented. This was done because the metric chosen to compare BLAST outputs was e-value, a measure of probability of matching a query to its target by chance (essentially calculating the likelihood of a false positive) whose calculation is dependent upon the size of the database being queried. Therefore, querying a consistently sized database is critical for e-value comparisons across searches.

>the maximum number of high-scoring segment pairs (HSPs) (−max_hsps_per_subject) was set to 1, but the maximum number of target sequences (−max_target_seqs) was not. This ensured that the BLAST output only reported the most significantly similar match between each contig and the query that matched it, but allowed multiple contigs to be matched by the same known query.

>The e-value threshold was set to 2, in order to capture sequences with low complexity that would be locally insignificant under a more stringent e-value setting, and to maximize the number of contigs identified.

>Any contig whose myxozoan e-value was at least three orders of magnitude smaller than its corresponding host e-value was selected and appended to the set that matched only to Myxozoa.

specie di riferimento per il dataset di anfipodi: *Hyalella azteca*. Per questo ho preso il fasta delle CDS da ncbi genomes.  

![idea per come fare con Pseudorchomene](img/20200707143321.png)


#### test con ws=7

```
blastn -db ../Pseudorchomene_ORP_no_rRNA.fasta -query ../Hyalella_azteca_genome/GCF_000764305.1_Hazt_2.0_genomic.fna  -evalue 2 -word_size 7 -max_hsps 1 -outfmt 6 -out Pseudorchomene_vs_Hyalella_ws7 -num_threads 100
```

hit di blast per le due specie:

+ Hematodinium: 6691945
+ Hyalella: 388739

##### incrocio delle tabelle

tramite python + pandas

![20200708093817](img/20200708093817.png)

dell'intersezione 15643 sequenze sono recuperabili per evalue in Hyalella 3 ordini di grandezza inferiore a evalue in Hematodinium. Viceversa quelle ascrivibili ad Hematodinium sono 18414.

![20200708103928](img/20200708103928.png)

Creando un trascrittoma contenente le sequenze classificate per Hyalella e le sequenze che non allineano con nessuna delle due specie i risultati di BUSCOv3 contro arthtopoda sono:

```
C:50.2%[S:44.0%,D:6.2%],F:27.3%,M:22.5%,n:1066

```
Su questo si può fare il mapping per vedere se si toglie un po' del rumore di hematodinium, e effettivamente succede, ma rimane ancora. Questo forse perchè bisognerebbe filtrare anche le read prima di mapparle su trascrittoma - mappare le read su trascrittoma di Hematodinium e usare le **non mappate** per mappare sul trascrittoma di Pseudorchomene ripulito.  

###### Analisi di espressione

suddividendo i campioni tra contaminati e non contaminati sulla base della tabella dei valori di kraken, per cui contaminati hanno perc. > 0.1, se si usa la contaminazione del parassita come metadato nella MDS si ha

![20200708164502](img/20200708164502.png)

con una separazione netta tra contaminati e non. Siccome il mapping viene già da un trascrittoma che dovrebbe essere decontaminato possiamo sapere se le differenze sono le sequenze del parassita stesso o sono la risposta di Pseudorchomene al parassita? 

Facendo il test statistico sulla base di questo confronto si ha questa heatmap:

![20200708164726](img/20200708164726.png)

-------------------
-------------------


separando le sequenze comuni con una differenza di evalue di un solo ordine di grandezza si ha:

![20200708140727](img/20200708140727.png)

Molte sequenze sono assegnate a Hematodinium. 

![20200708143916](img/20200708143916.png)


Come terzo controllo assemblo il trascrittoma di *Hirondellea gigas* che è una specie molto vicina a pseudorchomene di cui ci sono le sequenze su SRA. 

Script per il download
```
#!/usr/bin/env bash
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/DRR086/DRR086603/DRR086603_1.fastq.gz -o DRR086603_Illumina_HiSeq_2500_paired_end_sequencing_of_SAMD00071030_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/DRR086/DRR086603/DRR086603_2.fastq.gz -o DRR086603_Illumina_HiSeq_2500_paired_end_sequencing_of_SAMD00071030_2.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR382/008/SRR3822238/SRR3822238_1.fastq.gz -o SRR3822238_Transcriptome_Sequencing_of_Hirondellea_gigas_1.fastq.gz
curl -L ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR382/008/SRR3822238/SRR3822238_2.fastq.gz -o SRR3822238_Transcriptome_Sequencing_of_Hirondellea_gigas_2.fastq.gz
```

le read sono state trimmate con fastp, filtrate tramite mapping su una collezione di sequenze mitocondriali e ribosomali di *Lysianassoidea* e poi normalizzate con bbnorm con min=5 e max=100. Le read sono state processate con fix_reads.sh e poi assemblate tramite orp. 

A trascrittoma assemblato, ripetendo l'operazione precedente ma considerando il dataset Hirondellea + Hyalella come una cosa sola, escludendo anche le sequenze senza hit in blast e filtrando le hit comuni per evalue 10 volte inferiori a Hematodinium si ottiene un trascrittoma con BUSCO (Arthropoda):

```
C:70.0%[S:55.6%,D:14.4%],F:19.8%,M:10.2%,n:1066
```

tuttavia facendo i mapping si vede ancora un bel grado di contaminazione:

![20200715145015](img/20200715145015.png)

la nuova idea è di usare solo le sequenze codificanti per creare il database e poi usare i set di sequenze tradotte per classificare queste sequenze:

![20200715145146](img/20200715145146.png)

uso direttamente genomi di alveolata e di crustacea. 
```
diamond blastp --db Pseudorchomene_database.dmnd --outfmt 6 sseqid evalue -e 2 --query ../crostacei/Crustacea_protein_80.fasta --out Crustacea_hits.tbl -c 64
```

## Spoiler
c'è contaminazione anche in Eusirus!
